//
//  BaseResponse.swift
//
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation

class BaseResponse: BaseMap {
    @objc var cabResponse: CabResponse = CabResponse()
    
    required init() { }
    override func map(map: JSON) {
        self.cabResponse.map(map: map)
        self.cabResponse <- map["cabResponse"]
        
    }
}

class CabResponse: BaseMap {
    var codResponse: Int = -1
    var mensResponse: String! = nil
    
    enum Keys: String, CodingKey {
        case codResponse = "codResponse"
        case mensResponse = "mensReponse"
    }
    
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        try container.encode(codResponse, forKey: .codResponse)
        try container.encode(mensResponse, forKey: .mensResponse)
        
    }
    
    required init() { }
    override func map(map: JSON) {
        self.codResponse <- map[Keys.codResponse.rawValue]
        self.mensResponse <- map[Keys.mensResponse.rawValue]
        
    }
}
