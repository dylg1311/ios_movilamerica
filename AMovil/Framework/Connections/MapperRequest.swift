//
//  MapperRequest.swift
//
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation

// MARK: BaseMap
public protocol MapDelegate {
    func map(map: JSON)
    
}

class BaseMap: NSObject, MapDelegate, Encodable {
    required override init() { }
    func encode(to encoder: Encoder) throws { }
    func map(map: JSON) { }
    
}

// MARK: MapperRequest
class MapperRequest: NSObject
{
    class func sendRequestGET<T: BaseMap>(_ endPoint: String, _ onSuccess: @escaping(T) -> Void, _ onError: @escaping(NSError) -> Void) {
        User.post2endPointWithGET(endPoint: endPoint, onSuccess: { response in
            let object: T = T.init()
            object.map(map: response)
            onSuccess(object)
        }, onError: onError)
    }
}
