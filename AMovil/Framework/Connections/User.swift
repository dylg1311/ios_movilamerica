//
//  User.swift
//
//  Created by Diego Yael on 1/30/19.
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation

//MARK: - Connection with services POST OR GET request

class User: NSObject
{
    enum ErrorManagement : Int
    {
        case noConnection = 124  //124
        case badRequest = 404 //404
        case unauthorized = 401   //401
        case forbidden = 403  //403
        case unhandleError = 100
        case noResponse = 444 //444
    }
    
    public class func post2endPointWithGET(endPoint : String, onSuccess: @escaping(JSON) -> Void, onError:@escaping(NSError) -> Void)
    {
        if let internetResponse : NSError = self.internetConnection() as? NSError, let error = internetResponse as NSError?
        {
            onError(error)
            return
        }
        
        debugPrint("POST: \(endPoint)")

        let url : URL = URL.init(string: endPoint)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if error != nil && data == nil
            {
                onError(error! as NSError)
                return;
            }
            
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! JSON
                
                debugPrint("JSON RESPONSE: \(json)")
                onSuccess(json)
                
            }catch let error{
                onError(error as NSError)
            }
            }.resume()
        
    }
    
    //MARK: - Private methods
    
    private class func internetConnection() -> Any
    {
        let manager = NetworkManager.start() as! NetworkManager
        
        if manager.networkStatus == kNetworkDisConnected
        {
            return NSError.init(domain: "Sin conexión a internet", code:ErrorManagement.noConnection.rawValue, userInfo: nil)
        }
        
        return true
    }
    
}
