
#import <Foundation/Foundation.h>
#import "Reachability.h"

typedef enum{
    kNetworkConnected,
    kNetworkDisConnected
}kNetworkStatus;

@protocol NetworkManagerDelegate <NSObject>

-(void)NetWorkConnectionDropped:(kNetworkStatus)netStatus;
-(void)NetWorkConnectionConnected:(kNetworkStatus)netStatus;

@end

@interface NetworkManager : NSObject{
    
}
+ (id)startManager;
@property (nonatomic) kNetworkStatus networkStatus;
@property (nonatomic,strong) Reachability *internetReachability;
@property (nonatomic ,strong) id <NetworkManagerDelegate> delegate;


@end
