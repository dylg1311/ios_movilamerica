//
//  Toast.h
//  Template
//
//  Copyright (c). All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 Muestra una alerta
 */
@interface Toast : UIView
/**
 Crea una alerta

 @return La alerta Toast creada
 */
+(Toast *)newInstance;
/**
 Borra el mensaje de una alerta

 @return La alerta Toast sin mensaje
 */
+(Toast *)shareInstance;
/**
 Define, anima y muestra la alerta Toast

 @param mensaje mensaje a mostrar
 */
+(void)toastWithMessage:(NSString *)mensaje;
/**
 Muestra la alerta Toast

 @param mensaje mensaje a mostrar
 */
-(void)showWithMessage:(NSString *)mensaje;

@end
