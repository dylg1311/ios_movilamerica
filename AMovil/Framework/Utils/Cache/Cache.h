
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kSharedCache [Cache sharedInstance]

@interface Cache : NSObject

@property (strong, atomic, readonly) NSString *path_permanentFiles;
@property (strong, atomic, readonly) NSString *path_noPermanentFiles;
@property (strong, atomic, readonly) NSString *path_images;
@property (strong, atomic, readonly) NSString *path_requestImages;
@property (strong, atomic, readonly) NSString *path_files2sync;

@property (strong, nonatomic, readonly) NSMutableArray *synch_items2sync;
@property (strong, nonatomic, readonly) NSDate *synch_lastSyncDate;

# pragma mark - Shared instance

+ (Cache *)sharedInstance;

# pragma mark - Image methods

- (UIImage *)imageWithURL:(NSString *)url;
- (void)saveImage:(UIImage *)image withKey:(NSString *)key;
- (UIImage *)imageWithKey:(NSString *)key;

# pragma mark - Tools

- (void)clearCache;

@end
