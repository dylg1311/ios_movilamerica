

#import "Cache.h"

#define kDirectoryImages @"imageFiles"

#define kNoPermanentDataLimit   20.0 * 1024.0 * 1024.0    // MB
#define kImageDataLimit         20.0 * 1024.0 * 1024.0    // MB

static Cache *instance;

@interface Cache ()

@end

@implementation Cache
{
    unsigned long long int size_imagesDirectory;
    
    NSMutableArray *arr_pathsImages;
}

# pragma mark - Shared instance

+ (Cache *)sharedInstance
{
    @synchronized(self)
    {
        if (!instance)
        {
            instance = [self new];
        }
    }
    
    return instance;
}

# pragma mark - Init

- (id)init
{
    if (self = [super init])
    {
        [self initialSetup];
    }
    
    return self;
}

- (void)initialSetup
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    
    _path_images = [documentPath stringByAppendingPathComponent:kDirectoryImages];
    [self createDirectoryPath:_path_images];
    size_imagesDirectory = [self folderSize:_path_images];
    arr_pathsImages = [self contentsOrderByDateOfDirector:_path_images];
}

- (void)createDirectoryPath:(NSString *)directoryPath
{
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    
    if (![defaultManager fileExistsAtPath:directoryPath isDirectory:NULL])
    {
        [defaultManager createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
}

# pragma mark - Image methods

- (UIImage *)imageWithURL:(NSString *)url
{
    UIImage *image = [self imageWithKey:url];
    
    if (!image)
    {
        NSURL *url2request = [NSURL URLWithString:url];
        NSData *data = [NSData dataWithContentsOfURL:url2request];
        image = [UIImage imageWithData:data];
    }
    
    return image;
}

- (void)saveImage:(UIImage *)image withKey:(NSString *)key
{
    if (!image) return;
    
    NSString *keyMD5 = [key MD5];
    NSString *path = [_path_images stringByAppendingPathComponent:keyMD5];
    
    NSData *data = UIImageJPEGRepresentation(image, 0.2);
    
    if (!data) return;
    
    BOOL write = [data writeToFile:path atomically:YES];
    
    if (!write) return;
    
    size_imagesDirectory += [data length];
    [arr_pathsImages addObject:keyMD5];
    
    int limiteIteraciones = (int)arr_pathsImages.count;
    
    while ((size_imagesDirectory > kImageDataLimit) && arr_pathsImages.count && limiteIteraciones)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [_path_images stringByAppendingPathComponent:arr_pathsImages.firstObject];
        
        BOOL success = [fileManager removeItemAtPath:path error:nil];
        
        if (success)
        {
            NSDictionary *fileDictionary = [fileManager attributesOfItemAtPath:path error:nil];
            size_imagesDirectory -= [fileDictionary fileSize];
            [arr_pathsImages removeObject:arr_pathsImages.firstObject];
        }
        
        limiteIteraciones -= 1;
    }
}

- (UIImage *)imageWithKey:(NSString *)key
{
    NSString *keyMD5 = [key MD5];
    NSString *path = [_path_images stringByAppendingPathComponent:keyMD5];
    
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    NSData *imageData = [defaultManager contentsAtPath:path];
    
    if(!imageData) return nil;
    
    UIImage *image = [UIImage imageWithData:imageData];
    
    return image;
}

# pragma mark - Tools

- (void)clearCache
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL result_images = [fileManager removeItemAtPath:_path_images error:nil];
    
    NSLog(@"result_images: %d", result_images);
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef synchronize];
    
    [self initialSetup];
}

- (NSMutableArray *)contentsOrderByDateOfDirector:(NSString *)directoryPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error = nil;
    NSArray *filesArray = [fileManager contentsOfDirectoryAtPath:directoryPath error:&error];
    
    if (error != nil)
    {
        NSLog(@"Error in reading files: %@", [error localizedDescription]);
        return nil;
    }
    
    NSArray *sortedFiles = [filesArray sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
        
        NSString *filePath_1 = [directoryPath stringByAppendingPathComponent:obj1];
        NSDictionary *properties_1 = [fileManager attributesOfItemAtPath:filePath_1 error:nil];
        NSDate *modDate_1 = [properties_1 objectForKey:NSFileModificationDate];
        
        NSString *filePath_2 = [directoryPath stringByAppendingPathComponent:obj2];
        NSDictionary *properties_2 = [fileManager attributesOfItemAtPath:filePath_2 error:nil];
        NSDate *modDate_2 = [properties_2 objectForKey:NSFileModificationDate];
        
        return [modDate_1 compare:modDate_2];
    }];
    
    return [sortedFiles mutableCopy];
}

- (unsigned long long int)folderSize:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *filesArray = [fileManager subpathsOfDirectoryAtPath:path error:nil];
    unsigned long long int fileSize = 0;
    
    for (NSString *current in filesArray)
    {
        NSString *item = [path stringByAppendingPathComponent:current];
        NSDictionary *file = [fileManager attributesOfItemAtPath:item error:nil];
        
        fileSize += [file fileSize];
    }
    
    return fileSize;
}

@end
