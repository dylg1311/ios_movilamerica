//
//  NSString+Additions.h
//  AppTemplate
//
//  Copyright (c). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Utils)

- (UIColor *)colorValue;
-(UIImage *)decodeBase64ToImage;
-(NSString*)MD5;

@end
