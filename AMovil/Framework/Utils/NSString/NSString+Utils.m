//
//  NSString+Additions.m
//  AppTemplate
//
//  Copyright (c). All rights reserved.
//

#import "NSString+Utils.h"

#import <CommonCrypto/CommonDigest.h>
#include <ifaddrs.h>
#include <arpa/inet.h>

@implementation NSString (Utils)

#pragma mark - Getters methods

-(UIImage *)decodeBase64ToImage
{
    NSData *data = [[NSData alloc] initWithBase64EncodedString:self options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return [UIImage imageWithData:data];
}

-(NSString *)getValidText
{
    if (![self isEqualToString:@""]) return @"";
    
    NSString *clean = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return clean;
}

- (UIColor *)colorValue
{
    if (!self)
    {
        return [UIColor clearColor];
    }
    
    NSString *cString = self.getValidText;
    cString = [cString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    if ([cString length] < 6) return [UIColor clearColor];
    
    NSRange range;
    range.location = 0;
    
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    unsigned int r, g, b;
    
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(NSString*)MD5
{
    const char *cStr = [self UTF8String];
    
    unsigned char digest[16];
    
    CC_MD5(cStr, (unsigned)strlen(cStr), digest); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

@end
