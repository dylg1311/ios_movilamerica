//
//  Operators.swift
//
//  Created by Diego Yael on 1/31/19.
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation

precedencegroup MapOperator {
    associativity: left
    higherThan: LogicalConjunctionPrecedence
    
}

// MARK: MapOperator
infix operator <- : MapOperator

func <- <T>(object: inout T, map: Any?) {
    if let raw = map as? T {
        object = raw
        
    } else if let dict: JSON = map as? JSON,
        let objectMappable: BaseMap = object as? BaseMap {
        objectMappable.map(map: dict)
        
    }
}

// MARK: MapArrayOperator
infix operator <> : MapOperator

func <> <T: BaseMap>(object: inout [T], map: Any?) {
    if let mapArray = map as? [Any] {
        mapArray.forEach({
            if let mapDict: JSON = $0 as? JSON {
                let obj = T.init()
                obj.map(map: mapDict)
                object.append(obj)
                
            }
        })
    } else if let mapString: String = map as? String,
        let data: Data = mapString.data(using: .utf8),
        let json = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves),
        let mapArray: [Any] = json as? [Any] {
        mapArray.forEach({
            if let mapDict: JSON = $0 as? JSON {
                let obj = T.init()
                obj.map(map: mapDict)
                object.append(obj)
                
            }
        })
    }
}
