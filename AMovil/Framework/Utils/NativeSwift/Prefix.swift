//
//  Prefix.swift
//
//  Created by Diego Yael on 1/31/19.
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation
import UIKit

public typealias JSON = [String : Any]
public typealias CabRequest = [String : Dictionary<String, Any>]

private var serviceURL = ""

enum serviceNames
{
    case serviceTypeStart(pathsLogin)
    
    var serviceType: serviceType
    {
        switch self
        {
            case .serviceTypeStart(let path):
                serviceURL = path.rawValue
                return .serviceTypeStart
        }
    }
}

enum pathsLogin: String
{
    case getMovies = "GetMovies"
    case getImages = "GetImage"
}

enum serviceType: String
{
    case serviceTypeStart = "Home"
}

enum enviromentType: String
{
    case enviromentTypeProduccion = "Produccion"
    case enviromentTypeStaging = "Staging"
    case enviromentTypeNoValue = "NoValue"
}

//MARK: - Declaracion tamaños

var screenSize : CGRect {
    get{
        return UIScreen.main.bounds
    }
}

//MARK: - Servicios

func getServiceForSection(enviroment : enviromentType, serviceName : serviceNames) -> String
{
    if let path = Bundle.main.path(forResource: "Enviroments", ofType: "plist"), let dic = NSDictionary(contentsOfFile: path) as? [String: Any]
    {
        for (_ , element) in dic.enumerated()
        {
            var enviromentPredefined : enviromentType = .enviromentTypeNoValue
            
            if element.value as? Bool ?? false && element.key == "Produccion"
            {
                enviromentPredefined = .enviromentTypeProduccion
            }else if element.value as? Bool ?? false && element.key == "Staging"
            {
                enviromentPredefined = .enviromentTypeStaging
            }
            
            if enviromentPredefined != .enviromentTypeNoValue
            {
                if let sectionDic = dic[serviceName.serviceType.rawValue] as? [String : Any], let url = sectionDic[enviromentPredefined.rawValue] as? String
                {
                    return url
                }
            }
        }
        
        if let sectionDic = dic[serviceName.serviceType.rawValue] as? JSON, let dictPaths = sectionDic[serviceURL] as? JSON, let url = dictPaths[enviroment.rawValue] as? String
        {
            return url
        }
    }
    
    return ""
}

//MARK: - UserDefaults

public var serviceMoviesDefaults = "serviceMoviesDefaults"
public var serviceMoviesDateDefaults = "serviceMoviesDateDefaults"
