//
//  UIView+Utils.h
//  AppTemplate
//
//  Copyright (c). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utils)


- (UICollectionViewCell *)collectionViewCell;

- (UITableViewCell *) tableViewCell;

- (void)addShadow;

- (void)addShadowWithCornerRadius:(CGFloat)radius;
- (void)rounded;
- (void)roundedToDiameter:(float)diameter;
- (void)roundedWithCornerRadius:(CGFloat)radius;
- (void)roundedWithCornerRadius:(CGFloat)radius borderWidth:(CGFloat)width borderColor:(UIColor *)borderColor;
- (void)roundedWithCornerRadius:(CGFloat)radius withShadow:(BOOL)shadow;

- (void)sizeToFitInHeight;

- (void)sizeToFitInHeightAnimated:(BOOL)animated;

- (void)sizeToFitInHeight_WithBottomPadding:(CGFloat)padding;

- (void)sizeToFitByLastView:(UIView *)lastView;

- (void)adjustViewsInVertical:(NSArray *)views withSpaces:(NSArray *)spaces withResize:(BOOL)resize;

- (void)adjustViewsInHorizontal:(NSArray *)views withSpaces:(NSArray *)spaces;

- (void)adjustInScrollView:(UIScrollView *)scrollView withRootView:(UIView *)rootView withKeyboardHeight:(NSInteger) kbheight;

- (UIViewController *)viewController;

- (UIImage *)takeSnapshot;

- (UIImage *)takeSnapshotWithQuality:(float)quality;

- (CGPoint)absolutePosition;

- (CGPoint)absoluteCenter;

- (CGFloat)minX;
- (CGFloat)midX;
- (CGFloat)maxX;
- (CGFloat)minY;
- (CGFloat)midY;
- (CGFloat)maxY;
- (CGFloat)width;
- (CGFloat)height;
- (void)roundingCorners:(UIRectCorner)corners radius:(CGFloat)radius;

- (UIViewController *)topViewController;
- (void)vibratingViewAnimationWithIntense:(CGFloat)value;

@end
