//
//  UIView+Utils.m
//  AppTemplate
//
//  Copyright (c). All rights reserved.
//

#import "UIView+Utils.h"

@implementation UIView (Utils)


-(UICollectionViewCell *) collectionViewCell
{
    UIView *view = self;
    while (view && ![view isKindOfClass:[UICollectionViewCell class]])
    {
        view = [view superview];
    };
    return (UICollectionViewCell *) view;
}

-(UITableViewCell *) tableViewCell
{
    UIView* view = self;
    while (view && ![view isKindOfClass:[UITableViewCell class]]){
        view = [view superview];
    };
    return (UITableViewCell *) view;
}


# pragma mark - DRAW SHADOW

- (void)addShadow{
    
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(-15, 20);
    self.layer.shadowRadius = 5;
    self.layer.shadowOpacity = 0.5;
    
}

- (void)addShadowWithCornerRadius:(CGFloat)radius{
    
    self.layer.masksToBounds = NO;
    self.layer.cornerRadius = radius;
    self.layer.shadowOffset = CGSizeMake(5, 5);
    self.layer.shadowRadius = 4;
    self.layer.shadowOpacity = 0.5;
    
}


# pragma mark - ROUND METHODS

- (void)rounded{
    
    CGPoint saveCenter = self.center;
    CGRect newFrame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.width);
    self.frame = newFrame;
    self.layer.cornerRadius = self.frame.size.width / 2.0487234567;
    self.layer.masksToBounds = YES;
    self.center = saveCenter;
    
}

- (void)roundedToDiameter:(float)diameter{
    
    CGPoint saveCenter = self.center;
    CGRect newFrame = CGRectMake(self.frame.origin.x, self.frame.origin.y, diameter, diameter);
    self.frame = newFrame;
    self.layer.cornerRadius = diameter / 2.0487234567;
    self.layer.masksToBounds = YES;
    self.center = saveCenter;
    
}

- (void)roundedWithCornerRadius:(CGFloat)radius{
    
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
    
}

- (void)roundedWithCornerRadius:(CGFloat)radius withShadow:(BOOL)shadow{
    
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
    
    if(shadow) [self addShadowWithCornerRadius:radius];
    
}

- (void)roundedWithCornerRadius:(CGFloat)radius borderWidth:(CGFloat)width borderColor:(UIColor *)borderColor
{
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = width;
    self.layer.borderColor = borderColor.CGColor;
}


# pragma mark - SIZE TO FIT ON HEIGHT

- (void)sizeToFitInHeight{
    
    UIView *view = [[UIView alloc] init];
    
    for(UIView *subview in [self subviews]){
        
        if(subview.frame.origin.y > view.frame.origin.y){
            
            view = subview;
        }
    }
    
    CGRect selfFrame = self.frame;
    selfFrame.size.height = CGRectGetMaxY(view.frame) + 10; // view.frame.origin.y + view.frame.size.height;
    self.frame = selfFrame;
    
    view = nil;
}

- (void)sizeToFitInHeightAnimated:(BOOL)animated{
    
    UIView *view = [[UIView alloc] init];
    
    for(UIView *subview in [self subviews]){
        
        if(subview.frame.origin.y > view.frame.origin.y){
            
            view = subview;
        }
    }
    
    CGRect selfFrame = self.frame;
    selfFrame.size.height = CGRectGetMaxY(view.frame) + 10; // view.frame.origin.y + view.frame.size.height;
    
    if(animated){
        
        [UIView animateWithDuration:0.3 animations:^{
            self.frame = selfFrame;
        }];
        
    }else{
        
        self.frame = selfFrame;
        
    }
    
    view = nil;
}

- (void)roundingCorners:(UIRectCorner)corners radius:(CGFloat)radius
{
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                           byRoundingCorners:corners
                                                 cornerRadii:(CGSize){radius, radius}].CGPath;
    self.layer.mask = maskLayer;
}

- (void)sizeToFitInHeight_WithBottomPadding:(CGFloat)padding{
    
    UIView *view = [[UIView alloc] init];
    
    for(UIView *subview in [self subviews]){
        
        if(subview.frame.origin.y > view.frame.origin.y){
            
            view = subview;
        }
    }
    
    CGRect selfFrame = self.frame;
    selfFrame.size.height = CGRectGetMaxY(view.frame) + padding; //   view.frame.origin.y + view.frame.size.height + padding;
    self.frame = selfFrame;
    
    view = nil;
}

- (void)sizeToFitByLastView:(UIView *)lastView{
    
    CGRect selfFrame = self.frame;
    selfFrame.size.height = CGRectGetMaxY(lastView.frame) + 10;
    self.frame = selfFrame;
    
}

# pragma mark - ADJUST VERTICALLY DYNAMIC CONTENT

- (void)adjustViewsInVertical:(NSArray *)views withSpaces:(NSArray *)spaces withResize:(BOOL)resize{
    
    if(spaces == nil || spaces.count == 0){
        NSLog(@"Falta arreglo de paddings");
        return;
    }
    
    for(int i=0; i<views.count; i++)
    {
        static int nextPositionY;
        
        UIView *view=[views objectAtIndex:i];
        
        if(i==0){
            nextPositionY=view.frame.origin.y;
        }
        
        CGRect newframe=view.frame;
        newframe.origin.y=nextPositionY;
        view.frame=newframe;
        
        if(spaces==NULL){
            nextPositionY=view.frame.origin.y+view.frame.size.height;
        }else{
            NSNumber *spaceNum=[spaces objectAtIndex:i];
            nextPositionY=view.frame.origin.y+view.frame.size.height + [spaceNum intValue];
        }
        
    }
    
    if(resize) [self sizeToFitInHeight];
    
}

# pragma mark - ADJUST HORIZONTALLY DYNAMIC CONTENT

- (void)adjustViewsInHorizontal:(NSArray *)views withSpaces:(NSArray *)spaces{
    
    if(spaces == nil || spaces.count == 0){
        NSLog(@"Falta arreglo de paddings");
        return;
    }
    
    for(int i=0; i<views.count; i++)
    {
        static int nextPositionX;
        
        UIView *view=[views objectAtIndex:i];
        
        if(i==0){
            nextPositionX = view.frame.origin.x;
        }
        
        CGRect newframe = view.frame;
        newframe.origin.x = nextPositionX;
        view.frame = newframe;
        
        if(spaces==NULL){
            nextPositionX = view.frame.origin.x + view.frame.size.width;
        }else{
            NSNumber *spaceNum = [spaces objectAtIndex:i];
            nextPositionX = view.frame.origin.x + view.frame.size.width + [spaceNum intValue];
        }
        
    }
    
}

#pragma  mark - ADJUST VIEW WITH KEYBOARD

/*
 Si un view es tapado al salir el teclado, esta funcion cambia el inset de un scrollview dado
 para acomodar el view arriba del teclado.
 
 Se recomienda utilizarlo dentro de:
 keyboardWasShown:
 para obtener el tamaño del teclado facilmente, por ejemplo:
 
 - (void)keyboardWasShown:(NSNotification*)aNotification
 {
 NSDictionary* info = [aNotification userInfo];
 CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
 ...
 
 
 */

- (void)adjustInScrollView:(UIScrollView *)scrollView withRootView:(UIView *)rootView withKeyboardHeight:(NSInteger) kbheight
{
    NSInteger scrollAbsoluteY = [self convertPoint:CGPointZero fromView:scrollView].y; // pocision absoluta en el scrollview
    NSInteger windowAbsoluteY = [self convertPoint:CGPointZero fromView:rootView].y; // pocision absoluta en la pantalla
    
    NSInteger keyboardY = rootView.frame.size.height - kbheight;
    
    if(windowAbsoluteY + self.frame.size.height > keyboardY)
    {
        [UIView animateWithDuration:0.3 animations:^{
            [scrollView setContentInset:UIEdgeInsetsMake(0.0, 0.0, kbheight, 0.0)];
            
            if(scrollAbsoluteY - kbheight < scrollView.contentOffset.y)
            {
                [scrollView setContentOffset:CGPointMake(0, scrollAbsoluteY)];
            }
            else
            {
                [scrollView setContentOffset:CGPointMake(0, scrollAbsoluteY - kbheight)];
            }
        } completion:nil];
    }
}



# pragma mark - GET CONTROLLER METHOD

- (UIViewController *)viewController{
    
    for (UIView* next = [self superview];
         next;
         next = next.superview){
        
        UIResponder* nextResponder = [next nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]]){
            
            return (UIViewController*)nextResponder;
            
        }
        
    }
    
    return nil;
    
}

-(UIViewController *)topViewController
{
    UIViewController *topController = [[UIApplication sharedApplication].delegate window].rootViewController;
    
    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


# pragma mark - TAKE SNAPSHOT

- (UIImage *)takeSnapshot{
    
    return [self takeSnapshotWithQuality:1.0];
}

- (UIImage *)takeSnapshotWithQuality:(float)quality{
    
    quality = quality > 1.0 ? 1.0 : quality;
    
    CGRect frame = self.frame;
    
    CGSize size = frame.size;
    size.width *= quality;
    size.height *= quality;
    
    frame.size = size;
    
    UIGraphicsBeginImageContext(size);
    [self drawViewHierarchyInRect:frame afterScreenUpdates:NO];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}


#pragma mark - Absolute Position

- (CGPoint)absolutePosition{
    
    CGPoint absoluteOrigin = CGPointZero;
    
    //NSInteger parentAbsoluteX = 0.0; //pocisión absoluta X
    //NSInteger parentAbsoluteY = 0.0; //pocisión absoluta Y
    
    UIView *actualView = self;
    
    while(actualView){
        
        absoluteOrigin.x += actualView.frame.origin.x;
        absoluteOrigin.y += actualView.frame.origin.y;
        
        actualView = actualView.superview;
        
    }
    
    return absoluteOrigin;
    
}

#pragma mark - Absolute Position

- (CGPoint)absoluteCenter
{
    CGRect absoluteFrame = CGRectZero;
    
    CGPoint absoluteOrigin = [self absolutePosition];
    
    absoluteFrame.origin = absoluteOrigin;
    absoluteFrame.size = self.frame.size;
    
    return CGPointMake(CGRectGetMidX(absoluteFrame), CGRectGetMidY(absoluteFrame));
}

- (CGFloat)minX
{
    return CGRectGetMinX(self.frame);
}

- (CGFloat)midX
{
    return CGRectGetMidX(self.frame);
}

- (CGFloat)maxX
{
    return CGRectGetMaxX(self.frame);
}

- (CGFloat)minY
{
    return CGRectGetMinY(self.frame);
}

- (CGFloat)midY
{
    return CGRectGetMidY(self.frame);
}

- (CGFloat)maxY
{
    return CGRectGetMaxY(self.frame);
}

- (CGFloat)width
{
    return CGRectGetWidth(self.frame);
}

- (CGFloat)height
{
    return CGRectGetHeight(self.frame);
}

#pragma mark - Animations

- (void)vibratingViewAnimationWithIntense:(CGFloat)value
{
    if(value < 0)
        return;
    
    self.transform = CGAffineTransformMakeRotation(value);
    
    [UIView animateWithDuration:0.1 animations:^{
        self.transform = CGAffineTransformIdentity;
    }completion:^(BOOL finished) {
        self.transform = CGAffineTransformMakeRotation(-value);
        [UIView animateWithDuration:0.2 animations:^{
            self.transform = CGAffineTransformIdentity;
        }completion:^(BOOL finished) {
            if(value > 0)
                [self vibratingViewAnimationWithIntense:value - 0.04];
        }];
    }];
}

@end
