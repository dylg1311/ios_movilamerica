//
//  AuthResponse.swift
//
//  Created by Diego Yael on 3/11/19.
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation
import UIKit

//All the user's information

class TopRatedResponse: BaseResponse
{
    public var response: [Movie] = []
    
    override func map(map: JSON)
    {
        super.map(map: map)
        self.response <> map["results"]
    }
}

class Movie: BaseMap, NSCoding
{
    public var moviePopularity : Double!
    public var movieVideo: Bool!
    public var moviePosterPath : String!
    public var movieId : Int!
    public var movieOriginalTitle : String!
    public var movieTitle : String!
    public var movieOverview : String!
    public var movieReleaseDate : String!
    public var movieVoteRating : Double!
    
    required init() {
    }
    
    override func map(map: JSON)
    {
        if let value = Double(map.parseValue(key:"vote_average"))
        {
            self.movieVoteRating = (value * 100.0) / 100.0
        }
        
        self.moviePopularity <- Double(map.parseValue(key:"popularity"))
        self.movieVideo <- map.parseValue(key:"video")
        self.moviePosterPath <- map.parseValue(key:"poster_path")
        self.movieId <- Int(map.parseValue(key:"id"))
        self.movieOriginalTitle <- map.parseValue(key:"original_title")
        self.movieTitle <- map.parseValue(key:"title")
        self.movieOverview <- map.parseValue(key:"overview")
        self.movieReleaseDate <- map.parseValue(key:"release_date")
        
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(moviePosterPath, forKey: "poster_path")
        aCoder.encode(movieId, forKey: "id")
        aCoder.encode(movieOriginalTitle, forKey: "original_title")
        aCoder.encode(movieTitle, forKey: "title")
        aCoder.encode(movieOverview, forKey: "overview")
        aCoder.encode(movieReleaseDate, forKey: "release_date")
        aCoder.encode(movieVoteRating, forKey: "vote_average")
    }
    
    convenience required init?(coder aDecoder: NSCoder)
    {
        self.init()
        
        if let id = aDecoder.decodeObject(forKey: "id") as? Int
        {
            self.movieId = id
        }

        if let rating = aDecoder.decodeObject(forKey: "vote_average") as? Double
        {
            self.movieVoteRating = rating
        }
    
        self.moviePosterPath = aDecoder.decodeObject(forKey: "poster_path") as? String
        self.movieOriginalTitle = aDecoder.decodeObject(forKey: "original_title") as? String
        self.movieTitle = aDecoder.decodeObject(forKey: "title") as? String
        self.movieOverview = aDecoder.decodeObject(forKey: "overview") as? String
        self.movieReleaseDate = aDecoder.decodeObject(forKey: "release_date") as? String
    }

}
