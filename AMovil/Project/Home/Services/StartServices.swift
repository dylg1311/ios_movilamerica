//
//  LoginServices.swift
//
//  Created by Diego Yael on 3/7/19.
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation

let api_key = "ca48bb76c104cb36e023589e8d5466d1"

class StartServices: NSObject
{
    public class func serviceStartGettingData(onSuccess: @escaping(TopRatedResponse) -> Void, onError: @escaping(NSError) -> Void) {
        let endPoint: String = getServiceForSection(enviroment: .enviromentTypeProduccion, serviceName: .serviceTypeStart(.getMovies))
        let finalEndPoint = String.init(format: endPoint, api_key)
        MapperRequest.sendRequestGET(finalEndPoint, onSuccess, onError)
    }
    
}
