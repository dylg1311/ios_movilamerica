//
//  TextFieldCell.swift
//  UPAX
//
//  Created by Diego Yael Luna Gasca on 7/21/19.
//  Copyright © 2019 gs. All rights reserved.
//

import Foundation
import UIKit

class MovieCell: UITableViewCell
{
    private var movie : Movie!
   
    @IBOutlet weak var view_bk: UIView!
    
    @IBOutlet weak var img_movie: UIImageView!
    
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_date: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.view_bk.rounded(withCornerRadius: 10)
    }
    
    func setupWithInfo(movie : Movie)
    {
        self.lb_title.text = movie.movieTitle
        self.lb_date.text = movie.movieReleaseDate
    }
    
}
