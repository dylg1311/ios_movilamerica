//
//  LoginVC.swift
//
//  Created by Diego Yael on 2/13/19.
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation
import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate
{
    
    @IBOutlet weak var tableView: UITableView!

    private var isAvailableRequest : Bool!
    private var isFirst : Bool!
    private var refresh : UIRefreshControl!
    private var arrTopRatesMovies : [Movie] = []
    
    //MARK: - Override methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.initSetup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)

        if isAvailableRequest
        {
            self.isFirst = false
            self.isAvailableRequest = false
            self.pull2refresh()
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async
        {
            let userDefaults = UserDefaults.standard
            if let encodedData = userDefaults.value(forKey: serviceMoviesDefaults) as? Data
            {
                self.arrTopRatesMovies = NSKeyedUnarchiver.unarchiveObject(with: encodedData) as! [Movie]
                DispatchQueue.main.async
                {
                    self.tableView.reloadData()
                }
            }else
            {
                DispatchQueue.main.async
                {
                    if self.isFirst
                    {
                        self.isFirst = false
                        self.pull2refresh()
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination.isKind(of: MovieIntVC.self)
        {
            let vc = segue.destination as! MovieIntVC;
            if let movie = sender as? Movie
            {
                vc.movie = movie
            }
        }
    }
    
    //MARK: - Setup
    
    func initSetup()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.isAvailableRequest = false
        self.isFirst = true
        
        let userDefaults = UserDefaults.standard
        if let date = userDefaults.value(forKey: serviceMoviesDateDefaults) as? Date
        {
            if let diff = Calendar.current.dateComponents([.hour], from: date, to: Date()).hour, diff > 24
            {
                isAvailableRequest = true
            }
        }
        
        self.tableView.alwaysBounceVertical = true;
        self.refresh = UIRefreshControl.init();
        
        self.refresh.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.tableView.addSubview(self.refresh);
    }
    
    @objc func pull2refresh()
    {
        self.tableView.setContentOffset(CGPoint.init(x: 0, y: -self.refresh.frame.height), animated: true)
        self.loadData()
    }
    
    
    @objc func loadData()
    {
        self.refresh.beginRefreshing()
        StartServices.serviceStartGettingData(onSuccess: { (response) in
            
            if response.response.count == 0
            {
                DispatchQueue.main.async
                {
                    self.refresh.endRefreshing();
                }
                return
            }
            
            DispatchQueue.global(qos: .userInitiated).sync
            {
                let userDefaults = UserDefaults.standard
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: response.response)
                userDefaults.set(encodedData, forKey: serviceMoviesDefaults)
                userDefaults.set(Date.init(), forKey: serviceMoviesDateDefaults)
                userDefaults.synchronize()
            }
            
            DispatchQueue.main.async
            {
                self.arrTopRatesMovies = response.response
                self.tableView.reloadData();
                self.refresh.endRefreshing();
            }
        }) { (error) in
            DispatchQueue.main.async {
                Toast.toast(withMessage: error.localizedDescription)
                self.refresh.endRefreshing();
            }
        }
    }
    
    //MARK: - UITableView delegate

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrTopRatesMovies.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "MovieCell"

        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MovieCell
        let movie = self.arrTopRatesMovies[indexPath.row]
        cell.setupWithInfo(movie: movie)
        
    
        if let image = Cache.sharedInstance()?.image(withKey: movie.moviePosterPath)
        {
            cell.img_movie.image = image
            return cell
        }
    
        let endPoint: String = getServiceForSection(enviroment: .enviromentTypeProduccion, serviceName: .serviceTypeStart(.getImages))
        let finalInput = String.init(format: endPoint, movie.moviePosterPath)
        
        self.downloadImageWithUrl(imgURL: finalInput) { (image) in
            if let img = image as? UIImage
            {
                DispatchQueue.main.async
                {
                    tableView.cellForRow(at: indexPath)
                    cell.img_movie.image = img
                    Cache.sharedInstance()?.save(img, withKey: movie.moviePosterPath)
                }
            }else
            {
                DispatchQueue.main.async
                {
                    cell.img_movie.image = #imageLiteral(resourceName: "icon_default")
                }
            }
        }
        
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.performSegue(withIdentifier: "SegueIntMovie", sender: self.arrTopRatesMovies[indexPath.row])
    }
    
    public func downloadImageWithUrl(imgURL : String, handler: @escaping(AnyObject) -> Void)
    {
        DispatchQueue.global(qos: .userInitiated).async
        {
            if let imageDownloaded = Cache.sharedInstance()?.image(withURL: imgURL)
            {
                DispatchQueue.main.async
                {
                    handler(imageDownloaded)
                }
            }else
            {
                DispatchQueue.main.async
                {
                    handler(NSNull.self)
                }
            }
        }
    }
}
