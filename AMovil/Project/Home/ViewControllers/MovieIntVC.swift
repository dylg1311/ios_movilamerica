//
//  MovieIntVC.swift
//  AMovil
//
//  Created by Diego Yael Luna Gasca on 8/30/19.
//  Copyright © 2019 Diego Yael Luna Gasca. All rights reserved.
//

import Foundation

class MovieIntVC: UIViewController
{
    public var movie : Movie!

    @IBOutlet weak var img_poster: UIImageView!
    
    @IBOutlet weak var lb_title: UILabel!
    @IBOutlet weak var lb_date: UILabel!
    @IBOutlet weak var lb_desc: UILabel!
    
    @IBOutlet weak var img_startOne: UIImageView!
    @IBOutlet weak var img_starTwo: UIImageView!
    @IBOutlet weak var img_startThree: UIImageView!
    @IBOutlet weak var img_starFour: UIImageView!
    @IBOutlet weak var img_starFive: UIImageView!
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        lb_title.text = movie.movieTitle
        lb_date.text = movie.movieReleaseDate
        lb_desc.text = movie.movieOverview

        if let image = Cache.sharedInstance()?.image(withKey: movie.moviePosterPath)
        {
            img_poster.image = image
        }
        else
        {
            img_poster.image = #imageLiteral(resourceName: "icon_default")
        }
        
        if movie.movieVoteRating >= 0.1
            && movie.movieVoteRating <= 3
        {
            img_startOne.image = #imageLiteral(resourceName: "icon_star_yellow")
        }else if movie.movieVoteRating >= 3.1
            && movie.movieVoteRating <= 5
        {
            img_startOne.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_starTwo.image = #imageLiteral(resourceName: "icon_star_yellow")
        }else if movie.movieVoteRating >= 5.1
            && movie.movieVoteRating <= 7
        {
            img_startOne.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_starTwo.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_startThree.image = #imageLiteral(resourceName: "icon_star_yellow")
        }else if movie.movieVoteRating >= 7.1
            && movie.movieVoteRating <= 9
        {
            img_startOne.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_starTwo.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_startThree.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_starFour.image = #imageLiteral(resourceName: "icon_star_yellow")
        }else if movie.movieVoteRating >= 9.1
            && movie.movieVoteRating <= 10
        {
            img_startOne.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_starTwo.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_startThree.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_starFour.image = #imageLiteral(resourceName: "icon_star_yellow")
            img_starFive.image = #imageLiteral(resourceName: "icon_star_yellow")
        }
    }
}
