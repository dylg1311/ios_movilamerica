//
//  SplashVC.swift
//
//  Created by Diego Yael on 2/13/19.
//  Copyright © 2019 Diego Yael. All rights reserved.
//

import Foundation
import UIKit

class SplashVC: UIViewController
{
    
    @IBOutlet weak var progress: UIProgressView!
    
    //MARK: - Override methods
   
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.updateProgressBar()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)

        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute:{
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()

            self.present(vc!, animated: true, completion: nil)
        })
    }
    
    func updateProgressBar()
    {
        self.progress.setProgress(2.0, animated: false)
        UIView.animate(withDuration: 2.0, animations: {() -> Void in
            self.progress.layoutIfNeeded()
        })
    }
}
