//
//  AppDelegate.swift
//  AMovil
//
//  Created by Diego Yael Luna Gasca on 8/30/19.
//  Copyright © 2019 Diego Yael Luna Gasca. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

